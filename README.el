(setq package-list '(
all-the-icons
all-the-icons-completion
all-the-icons-dired
all-the-icons-ibuffer
auctex
avy
cider
consult
consult-dir
consult-flycheck
consult-projectile
consult-yasnippet
dashboard
doom-modeline
ein
embark
embark-consult
flycheck
gruvbox-theme
htmlize
hyperbole
lua-mode
magit
marginalia
markdown-mode
(mixed-pitch :type git :host gitlab :repo "jabranham/mixed-pitch" :fork (:host gitlab :repo "zmberber/mixed-pitch" :branch "17-weight-height-options"))
multi-vterm
nix-mode
orderless
org
org-modern
(org-phscroll :type git :host github :repo "misohena/phscroll")
org-ql
ox-gfm
password-store
pdf-tools
projectile
reformatter ; needed for nix-mode
rust-mode
smartparens
vertico
vterm
yaml-mode
yasnippet
yasnippet-snippets
))

(mapcar 'straight-use-package package-list)

(setq custom-file "~/.config/emacs/custom.el")
(when (file-exists-p custom-file)
  (load custom-file))

(setq vc-handled-backends nil)

(setq backup-by-copying t)

(setq  version-control t)

(setq vc-make-backup-files t)

(let ((backup-directory (concat user-emacs-directory "backup"))
      (save-files-directory (concat user-emacs-directory "auto-save")))
  (setq backup-directory-alist
	`((".*" . ,backup-directory)))
  (make-directory backup-directory :parents)
  (setq auto-save-file-name-transforms
      `((".*" ,save-files-directory t)))
  (make-directory save-files-directory :parents))

(setq delete-old-versions t
  kept-new-versions 6
  kept-old-versions 2
  version-control t)

(setq create-lockfiles nil)

(with-eval-after-load 'tramp-archive (setq tramp-archive-enabled nil))

(pdf-tools-install)

(setq custom-raised-buttons nil)

(global-set-key (kbd "C-x g") 'magit-status)

(require 'projectile)
(projectile-mode +1)

(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

(require 'dired-x)

(add-hook 'dired-mode-hook
    (lambda ()
        (dired-hide-details-mode)))

(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)

(put 'dired-find-alternate-file 'disabled nil)

(setq dired-listing-switches "-alth")

(vertico-mode 1)

(setq completion-styles '(orderless basic)
      completion-category-overrides '((file (styles basic partial-completion))))

(setq completion-ignore-case t)
(setq read-file-name-completion-ignore-case t)
(setq read-buffer-completion-ignore-case t)

;; Add prompt indicator to `completing-read-multiple'.
;; Alternatively try `consult-completing-read-multiple'.
(defun crm-indicator (args)
  (cons (concat "[CRM] " (car args)) (cdr args)))
(advice-add #'consult-completing-read-multiple :filter-args #'crm-indicator)
;; Do not allow the cursor in the minibuffer prompt
(setq minibuffer-prompt-properties
      '(read-only t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
;; Emacs 28: Hide commands in M-x which do not work in the current mode.
;; Vertico commands are hidden in normal buffers.
(setq read-extended-command-predicate
      #'command-completion-default-include-p)
;; Enable recursive minibuffers
(setq enable-recursive-minibuffers t)

(defun +embark-live-vertico ()
  "Shrink Vertico minibuffer when `embark-live' is active."
  (when-let (win (and (string-prefix-p "*Embark Live" (buffer-name))
                      (active-minibuffer-window)))
    (with-selected-window win
      (when (and (bound-and-true-p vertico--input)
                 (fboundp 'vertico-multiform-unobtrusive))
        (vertico-multiform-unobtrusive)))))

(add-hook 'embark-collect-mode-hook #'+embark-live-vertico)

(advice-add #'vertico--format-candidate :around
            (lambda (orig cand prefix suffix index _start)
              (setq cand (funcall orig cand prefix suffix index _start))
              (concat
               (if (= vertico--index index)
                   (propertize "» " 'face 'vertico-current)
                 "  ")
               cand)))

;; Configure the default sorting function for symbols and files
;; See `vertico-sort-function'.
(setq vertico-multiform-categories
      '((symbol (vertico-sort-function . vertico-sort-alpha))
        (file (vertico-sort-function . sort-directories-first))))

;; Forcibly override the sorting function for `consult-line'.
;; See `vertico-sort-override-function'.
(setq vertico-multiform-commands
      '((consult-line (vertico-sort-override-function . vertico-sort-alpha))))

(defun sort-directories-first (files)
  ;; Still sort by history position, length and alphabetically
  (setq files (vertico-sort-history-length-alpha files))
  ;; But then move directories first
  (nconc (seq-filter (lambda (x) (string-suffix-p "/" x)) files)
         (seq-remove (lambda (x) (string-suffix-p "/" x)) files)))

(defvar +vertico-transform-functions nil)

(defun +vertico-transform (args)
  (dolist (fun (ensure-list +vertico-transform-functions) args)
    (setcar args (funcall fun (car args)))))

(advice-add #'vertico--format-candidate :filter-args #'+vertico-transform)

(defun +vertico-highlight-directory (file)
  "Highlight FILE if it ends with a slash."
  (if (string-suffix-p "/" file)
      (propertize file 'face 'marginalia-file-priv-dir)
    file))

(setq vertico-multiform-commands
      '(("find-file" flat
         (vertico-sort-function . sort-directories-first)
         (+vertico-transform-functions . +vertico-highlight-directory))))

;; Taken from marginalia-annotate-binding
(defun +vertico-annotate-binding (command)
  "Annotate COMMAND with key binding in flat/unobtrusive mode."
  (if-let* (((or (bound-and-true-p vertico-flat-mode)
                 (bound-and-true-p vertico-unobtrusive-mode)))
            (sym (intern-soft command))
            (key (and (commandp sym) (where-is-internal sym nil 'first-only))))
      (format #("%s (%s)" 3 7 (face shadow)) command (key-description key))
    command))

(setq vertico-multiform-commands
      '(("\\`execute-extended-command" unobtrusive
         (+vertico-transform-functions . +vertico-annotate-binding))))

(defun down-from-outside ()
  "Move to next candidate in minibuffer, even when minibuffer isn't selected."
  (interactive)
  (with-selected-window (active-minibuffer-window)
    (execute-kbd-macro [down])))

(defun up-from-outside ()
  "Move to previous candidate in minibuffer, even when minibuffer isn't selected."
  (interactive)
  (with-selected-window (active-minibuffer-window)
    (execute-kbd-macro [up])))

(defun to-and-fro-minibuffer ()
  "Go back and forth between minibuffer and other window."
  (interactive)
  (if (window-minibuffer-p (selected-window))
      (select-window (minibuffer-selected-window))
    (select-window (active-minibuffer-window))))

(defun vertico-directory-delete-entry ()
  "Delete directory or entire entry before point."
  (interactive)
  (when (and (> (point) (minibuffer-prompt-end))
             ;; Check vertico--base for stepwise file path completion
             (not (equal vertico--base ""))
             (eq 'file (vertico--metadata-get 'category)))
    (save-excursion
      (goto-char (1- (point)))
      (when (search-backward "/" (minibuffer-prompt-end) t)
        (delete-region (1+ (point)) (point-max))
        t))))

(defadvice vertico-insert
    (after vertico-insert-add-history activate)
  "Make vertico-insert add to the minibuffer history."
  (unless (eq minibuffer-history-variable t)
    (add-to-history minibuffer-history-variable (minibuffer-contents))))

(defvar previous-directory nil
    "The directory that was just left. It is set when leaving a directory and
    set back to nil once it is used in the parent directory.")

(defun set-previous-directory ()
  "Set the directory that was just exited from within find-file."
  (when (> (minibuffer-prompt-end) (point))
    (save-excursion
      (goto-char (1- (point)))
      (when (search-backward "/" (minibuffer-prompt-end) t)
        ;; set parent directory
        (setq previous-directory (buffer-substring (1+ (point)) (point-max)))
        ;; set back to nil if not sorting by directories or what was deleted is not a directory
        (when (not (string-suffix-p "/" previous-directory))
          (setq previous-directory nil))
        t))))

(advice-add #'vertico-directory-up :before #'set-previous-directory)

(define-advice vertico--update (:after (&rest _) choose-candidate)
    "Pick the previous directory rather than the prompt after updating candidates."
    (cond
     (previous-directory ; select previous directory
      (setq vertico--index (or (seq-position vertico--candidates previous-directory)
                               vertico--index))
      (setq previous-directory nil))))

(defun my/vertico-truncate-candidates (args)
  (if-let ((arg (car args))
           (type (get-text-property 0 'multi-category arg))
           ((eq (car-safe type) 'file))
           (w (max 30 (- (window-width) 38)))
           (l (length arg))
           ((> l w)))
      (setcar args (concat "…" (truncate-string-to-width arg l (- l w)))))
  args)
(advice-add #'vertico--format-candidate :filter-args #'my/vertico-truncate-candidates)

(advice-add #'vertico-next
            :around
            #'(lambda (origin &rest args)
                (let ((beg-index vertico--index))
                  (apply origin args)
                  (if (not (eq 1 (abs (- beg-index vertico--index))))
                      (ding)))))

(defun my-vertico-insert-unless-tramp ()
  "Insert current candidate in minibuffer, except for tramp."
  (interactive)
  (if (vertico--remote-p (vertico--candidate))
      (minibuffer-complete)
    (vertico-insert)))

(savehist-mode 1)

(recentf-mode 1)

(run-at-time (current-time) 300 'recentf-save-list)

(global-set-key (kbd "C-c h") 'consult-history)
(global-set-key (kbd "C-c m") 'consult-mode-command)
(global-set-key (kbd "C-c k") 'consult-kmacro)
;; C-x bindings (ctl-x-map)
(global-set-key (kbd "C-x M-:") 'consult-complex-command)     ;; orig. repeat-complex-command
(global-set-key (kbd "C-x b") 'consult-buffer)                ;; orig. switch-to-buffer
(global-set-key (kbd "C-x 4 b") 'consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
(global-set-key (kbd "C-x 5 b") 'consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
(global-set-key (kbd "C-x r b") 'consult-bookmark)            ;; orig. bookmark-jump
(global-set-key (kbd "C-x p b") 'consult-project-buffer)      ;; orig. project-switch-to-buffer
;; Custom M-# bindings for fast register access
(global-set-key (kbd "M-#") 'consult-register-load)
(global-set-key (kbd "M-'") 'consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
(global-set-key (kbd "C-M-#") 'consult-register)
;; Other custom bindings
(global-set-key (kbd "M-y") 'consult-yank-pop)                ;; orig. yank-pop
;; M-g bindings (goto-map)
(global-set-key (kbd "M-g e") 'consult-compile-error)
(global-set-key (kbd "M-g f") 'consult-flymake)               ;; Alternative: consult-flycheck
(global-set-key (kbd "M-g g") 'consult-goto-line)             ;; orig. goto-line
(global-set-key (kbd "M-g M-g") 'consult-goto-line)           ;; orig. goto-line
(global-set-key (kbd "M-g o") 'consult-outline)               ;; Alternative: consult-org-heading
(global-set-key (kbd "M-g m") 'consult-mark)
(global-set-key (kbd "M-g k") 'consult-global-mark)
(global-set-key (kbd "M-g i") 'consult-imenu)
(global-set-key (kbd "M-g I") 'consult-imenu-multi)
;; M-s bindings (search-map)
(global-set-key (kbd "M-s d") 'consult-find)
(global-set-key (kbd "M-s D") 'consult-locate)
(global-set-key (kbd "M-s g") 'consult-grep)
(global-set-key (kbd "M-s G") 'consult-git-grep)
(global-set-key (kbd "M-s r") 'consult-ripgrep)
(global-set-key (kbd "M-s l") 'consult-line)
(global-set-key (kbd "M-s L") 'consult-line-multi)
(global-set-key (kbd "M-s k") 'consult-keep-lines)
(global-set-key (kbd "M-s u") 'consult-focus-lines)
;; Isearch integration
(global-set-key (kbd "M-s e") 'consult-isearch-history)
(define-key isearch-mode-map (kbd "M-e") 'consult-isearch-history)         ;; orig. isearch-edit-string
(define-key isearch-mode-map (kbd "M-s e") 'consult-isearch-history)       ;; orig. isearch-edit-string
(define-key isearch-mode-map (kbd "M-s l") 'consult-line)                  ;; needed by consult-line to detect isearch
(define-key isearch-mode-map (kbd "M-s L") 'consult-line-multi)            ;; needed by consult-line to detect isearch
;; Minibuffer history
(define-key minibuffer-local-map (kbd "M-s") 'consult-history)                 ;; orig. next-matching-history-element
(define-key minibuffer-local-map (kbd "M-r") 'consult-history)               ;; orig. previous-matching-history-element

(global-set-key (kbd "C-x b") 'consult-buffer)

(global-set-key (kbd "C-x C-d") 'consult-dir)
(define-key vertico-map (kbd "C-x C-d") 'consult-dir)
(define-key vertico-map (kbd "C-x C-j") 'consult-dir-jump-file)

(marginalia-mode 1)

(define-key minibuffer-local-map (kbd "M-A") 'marginalia-cycle)

(add-hook 'marginalia-mode-hook #'all-the-icons-completion-marginalia-setup)

(global-set-key (kbd "C-.") 'embark-act)
(global-set-key (kbd "C-;") 'embark-dwim)
(global-set-key (kbd "C-h B") 'embark-bindings)

(setq prefix-help-command #'embark-prefix-help-command)

(add-hook 'embark-collect-mode-hook #'consult-preview-at-point-mode)

(all-the-icons-completion-mode 1)

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

(modify-all-frames-parameters
 '((right-divider-width . 8)
   (internal-border-width . 8)))
(custom-set-faces
 '(window-divider-first-pixel ((t (:inherit window-divider))))
 '(window-divider-last-pixel ((t (:inherit window-divider)))))
(set-face-background 'fringe (face-attribute 'default :background))

(add-hook 'after-init-hook #'doom-modeline-mode)

(setq tab-bar-show nil)
(setq doom-modeline-workspace-name t)

(column-number-mode 1)

(setq use-dialog-box nil)

(blink-cursor-mode 0)

(set-default 'truncate-lines nil)

(setq-default word-wrap t)

(when (display-graphic-p)
  (require 'all-the-icons)
  (require 'nerd-icons))
(unless (file-exists-p "~/.local/share/fonts/all-the-icons.ttf")
  (all-the-icons-install-fonts))
(unless (file-exists-p "~/.local/share/fonts/NFM.ttf")
  (nerd-icons-install-fonts))

(dashboard-setup-startup-hook)

(setq dashboard-items '((recents  . 5)
                        (bookmarks . 5)
                        (projects . 5)
                        (agenda . 5)
                        (registers . 5)))

;; (setq dashboard-set-navigator t)
;; (setq dashboard-navigator-buttons
;;       `(;; line1
;;         ((,(all-the-icons-octicon "mark-github" :height 1.1 :v-adjust 0.0)
;;          "Homepage"
;;          "Browse homepage"
;;          (lambda (&rest _) (browse-url "homepage")))
;;         ("★" "Star" "Show stars" (lambda (&rest _) (show-stars)) warning)
;;         ("?" "" "?/h" #'show-help nil "<" ">"))
;;          ;; line 2
;;         ((,(all-the-icons-faicon "linkedin" :height 1.1 :v-adjust 0.0)
;;           "Linkedin"
;;           ""
;;           (lambda (&rest _) (browse-url "homepage")))
;;          ("⚑" nil "Show flags" (lambda (&rest _) (message "flag")) error))))

(setq dashboard-footer-messages '("Was ist geiler, Apfelsaft oder Orangensaft?"))

(setq dashboard-footer-icon (all-the-icons-octicon "heart"
                                                   :height 1.1
                                                   :v-adjust -0.05
                                                   :face 'font-lock-keyword-face))

(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))

(setq global-visual-line-mode t)
(setq line-move-visual nil)

(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-follow-mouse t)
(setq scroll-step 1)
(setq hscroll-step 1)

(global-set-key (kbd "C-/") 'avy-goto-char)

(require 'smartparens-config)

(show-smartparens-global-mode t)

(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)

(yas-global-mode 1)

(setq split-width-threshold 120)

(global-set-key (kbd "C-x k") 'kill-this-buffer)

(global-set-key (kbd "C-x C-b") 'ibuffer)

(all-the-icons-ibuffer-mode 1)

;; (defun my-all-the-icons-ibuffer ()
;;   (setq all-the-icons-ibuffer-formats
;; 	`((mark modified read-only locked
;; 		" " ,(if (and (display-graphic-p)
;; 			      all-the-icons-ibuffer-icon)
;; 			 '(icon 2 2 :left :elide)
;; 		       "")
;; 		,(if (and (display-graphic-p)
;; 			  all-the-icons-ibuffer-icon)
;; 		     (propertize " " 'display `(space :align-to 8))
;; 		   "")
;; 		(name 18 18 :left :elide)
;; 		" " (size-h 9 -1 :right)
;; 		" " (mode+ 16 16 :left :elide)
;; 		" " filename-and-process+)
;; 	  (mark " " (name 16 -1) " " filename))
;; 	)
;;   (setq ibuffer-formats all-the-icons-ibuffer-formats))
;; (add-hook 'server-after-make-frame-hook 'my-all-the-icons-ibuffer)
(add-hook 'ibuffer-mode-hook #'all-the-icons-ibuffer-mode)

(save-place-mode 1)

(setq ediff-window-setup-function 'ediff-setup-windows-plain)

(setq
 ;; Edit settings
 org-auto-align-tags nil
 org-tags-column 0)

(setq 
 ;; Org styling, hide markup etc.
 org-hide-emphasis-markers t
 org-pretty-entities t
 org-ellipsis "…"

 ;; Agenda styling
 org-agenda-tags-column 0
 org-agenda-block-separator ?─
 org-agenda-time-grid
 '((daily today require-timed)
   (800 1000 1200 1400 1600 1800 2000)
   " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
 org-agenda-current-time-string
 "⭠ now ─────────────────────────────────────────────────")

(custom-set-faces
 '(org-level-8 ((t (:height 1.15))))
 '(org-level-7 ((t (:inherit org-level-8 :height 1.0))))
 '(org-level-6 ((t (:inherit org-level-7 :height 1.0))))
 '(org-level-5 ((t (:inherit org-level-6 :height 1.0))))
 '(org-level-4 ((t (:inherit org-level-5 :height 1.0))))
 '(org-level-3 ((t (:inherit org-level-4 :height 1.15))))
 '(org-level-2 ((t (:inherit org-level-3 :height 1.15))))
 '(org-level-1 ((t (:inherit org-level-2 :height 1.15))))
'(org-document-title ((t (:height 2.0)))))

(add-hook 'org-mode-hook 'mixed-pitch-mode)

(global-org-modern-mode 1)

(setq org-modern-table nil)

(setq org-startup-truncated nil)

(with-eval-after-load "org"
  (require 'org-phscroll))

(setq org-startup-indented nil)
(setq org-adapt-indentation nil)
(setq org-src-preserve-indentation t)

(setq org-startup-folded t)

(setq org-file-apps
  '((auto-mode . emacs)
    (directory . emacs)
    ("\\.mm\\'" . default)
    ("\\.x?html?\\'" . default)
    ("\\.pdf\\'" . emacs)))

(eval-after-load "org"
  '(require 'ox-gfm nil t))

(setq org-latex-compiler "lualatex")

(setq org-time-stamp-custom-formats '("<%a, %Y %b %e>" . "<%a, %y %b %e, %H:%M:%S>"))

(defun my-org-export-ensure-custom-times (backend)
  (setq-local org-display-custom-times t))
(add-hook 'org-export-before-processing-hook 'my-org-export-ensure-custom-times)

(defun org-export-filter-timestamp-remove-brackets (timestamp backend info)
  "removes relevant brackets from a timestamp"
  (cond
   ((org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "[<>]\\|[][]" "" timestamp))
   ((org-export-derived-backend-p backend 'html)
    (replace-regexp-in-string "&[lg]t;\\|[][]" "" timestamp))))
(eval-after-load 'ox '(add-to-list
                       'org-export-filter-timestamp-functions
                       'org-export-filter-timestamp-remove-brackets))

(defun my/indent-buffer-then-save ()
  (indent-region (point-min) (point-max))
  (save-buffer))
(add-hook 'org-babel-post-tangle-hook 'my/indent-buffer-then-save)

(setq
 org-catch-invisible-edits 'show-and-error
 org-special-ctrl-a/e t
 org-insert-heading-respect-content t)

(define-key org-mode-map (kbd "C-c q") 'org-ql-find)

(setq TeX-auto-save t
      TeX-parse-self t)

(setq-default TeX-master nil)

(setq TeX-engine (quote luatex))

(setq TeX-save-query nil)

(setq exec-path (append exec-path '("/usr/bin/vendor_perl")))

(setq TeX-debug-bad-boxes t)
(setq TeX-debug-warnings t)

(setq TeX-source-correlate-mode t
      TeX-source-correlate-start-server t)

(setq TeX-view-program-selection '((output-pdf "PDF Tools")))

(add-hook 'TeX-after-compilation-finished-functions
          #'TeX-revert-document-buffer)

(require 'ein)

(setq ein:output-area-inlined-images t)

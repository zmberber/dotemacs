(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-source-save-behavior nil)
 '(custom-enabled-themes '(gruvbox-dark-medium))
 '(custom-safe-themes
   '("046a2b81d13afddae309930ef85d458c4f5d278a69448e5a5261a5c78598e012" "fa49766f2acb82e0097e7512ae4a1d6f4af4d6f4655a48170d0a00bcb7183970" "3e374bb5eb46eb59dbd92578cae54b16de138bc2e8a31a2451bf6fdb0f3fd81b" "19a2c0b92a6aa1580f1be2deb7b8a8e3a4857b6c6ccf522d00547878837267e7" "2ff9ac386eac4dffd77a33e93b0c8236bb376c5a5df62e36d4bfa821d56e4e20" "d80952c58cf1b06d936b1392c38230b74ae1a2a6729594770762dc0779ac66b7" "72ed8b6bffe0bfa8d097810649fd57d2b598deef47c992920aef8b5d9599eefe" "6b5c518d1c250a8ce17463b7e435e9e20faa84f3f7defba8b579d4f5925f60c1" "83e0376b5df8d6a3fbdfffb9fb0e8cf41a11799d9471293a810deb7586c131e6" "7661b762556018a44a29477b84757994d8386d6edee909409fabe0631952dad9" "939ea070fb0141cd035608b2baabc4bd50d8ecc86af8528df9d41f4d83664c6a" "aded61687237d1dff6325edb492bde536f40b048eab7246c61d5c6643c696b7f" "4cf9ed30ea575fb0ca3cff6ef34b1b87192965245776afa9e9e20c17d115f3fb" "123a8dabd1a0eff6e0c48a03dc6fb2c5e03ebc7062ba531543dfbce587e86f2a" "e1d09f1b2afc2fed6feb1d672be5ec6ae61f84e058cb757689edb669be926896" "a06658a45f043cd95549d6845454ad1c1d6e24a99271676ae56157619952394a" default))
 '(org-file-apps
   '((auto-mode . emacs)
     (directory . emacs)
     ("\\.mm\\'" . default)
     ("\\.x?html?\\'" . default)
     ("\\.pdf\\'" . emacs)))
 '(package-selected-packages
   '(hotfuzz orderless marginalia embark embark-consult consult-dir consult-flycheck consult-lsp consult-notmuch consult-projectile consult-yasnippet consult vertico all-the-icons-completion all-the-icons-dired all-the-icons-ibuffer all-the-icons-ivy doom-modeline lua-mode ein solidity-mode org-tree-slide epresent pyvenv flycheck-aspell password-store magit counsel ivy auctex hyperbole projectile vterm markdown-mode markdown-mode+ circe yasnippet-snippets yasnippet nix-mode simple-modeline yaml-mode xterm-color wrap-region smartparens pdf-tools idris-mode htmlize gruvbox-theme flx cdlatex avy))
 '(safe-local-variable-values
   '((ein:jupyter-server-command . "/home/berber/documents/kurse/machine_learning/mllab-exercise-3-team_tanuki/.mllab/bin/jupyter")
     (ein:jupyter-server-command . "/home/berber/documents/kurse/machine_learning/mllab-exercise-4-team_tanuki/.mllab/bin/jupyter")
     (ein:jupyter-server-command . "/home/berber/documents/arbeit/.venv/bin/jupyter"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-document-title ((t (:height 2.0))))
 '(org-level-1 ((t (:inherit org-level-2 :height 1.15))))
 '(org-level-2 ((t (:inherit org-level-3 :height 1.15))))
 '(org-level-3 ((t (:inherit org-level-4 :height 1.15))))
 '(org-level-4 ((t (:inherit org-level-5 :height 1.0))))
 '(org-level-5 ((t (:inherit org-level-6 :height 1.0))))
 '(org-level-6 ((t (:inherit org-level-7 :height 1.0))))
 '(org-level-7 ((t (:inherit org-level-8 :height 1.0))))
 '(org-level-8 ((t (:height 1.15))))
 '(window-divider-first-pixel ((t (:inherit window-divider))))
 '(window-divider-last-pixel ((t (:inherit window-divider)))))
